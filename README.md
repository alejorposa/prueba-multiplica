<h1 align="center">Welcome to Prueba Multiplica 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
  </a>
</p>

> El sistema permite listar los colores para un copiado rápido, llama a la API suministrada y se lista por paginación.

## Producción

* Url: https://prueba.jeanzum.now.sh/

## Git

* Gitlab: https://gitlab.com/alejorposa/prueba-multiplica.git

## Install

```sh
npm install
## Para instalar todas las dependencias del desarrollo. 
npm run dev
## Iniciar en modo desarrollo 
npm run build
##  Compilar el proyecto para producción
```
## Frameworks / Tecnologías

* VueJs
* Stylus
* Webpack
* Pug
* Babel


## Author

👤 **Alejandro Restrepo**

* Website: alejorestrepo.ml

## Show your support

Give a ⭐️ if this project helped you!

***