export default {
    name: 'colorDashboard',
    data() {
        return {
            page: 1,
            baseUrl: `https://reqres.in/api/colors/`,
            colors: [],
            total_pages: 0,
            per_page: 0,
            next_page: 0
        }
    },
    created(){
        this.getColors()
    },
    watch: {
        page(n){
            (n <= this.total_pages && n >= 0) ?  this.getColors() : this.page = this.next_page
        }
    },
    methods: {
        getColors(){
            this.$http.get(`${this.baseUrl}?page=${this.page}`)
            .then(response => {
                this.page = response.body.page
                this.next_page = this.page
                this.per_page = response.body.per_page
                this.total_pages = response.body.total_pages
                this.colors = response.body.data
            })
        },
        copyColor(hex, i) {
            navigator.clipboard.writeText(hex).then(() => {
                let name = document.getElementsByTagName('h4')
                name[i].style.display = 'none'
                this.colors[i].color = '¡Copiado!'

                setTimeout(() => {
                   this.colors[i].color = hex 
                   name[i].style.display = 'block'
                }, 1000)

                console.log('Copiado Correctamente')
            }, () => {
                console.log('No se pudo copiar')
            })
        }
    }
}